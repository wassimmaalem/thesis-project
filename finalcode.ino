#include "esp_deep_sleep.h"
#include <SPI.h>
#include <WiFiClientSecure.h>
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
#include <ESP32Ping.h>
#include <WiFi.h>

SSD1306  display(0x3C, 16, 17);    // IO16 and IO17 are SDA / SDA lines for I2C communication


//////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

const char* machine_id  = "98"; 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#define __DEMO

#ifdef __DEMO
  const char* host        = "192.168.0.119";
  const char* ssid        = "d19";
  const char* password    = "11335577";
#else 
  const char* host        = "intra.powerpak.eu";
  const char* ssid        = "LAPTOP 0509";
  const char* password    = "Powerpak2018";
#endif

String dispString[4];

void log( int n, String text) {
   dispString[n] = text;   
   display.clear();           
   display.drawString(0, 0, dispString[0]);
   display.drawString(0, 20,  dispString[1]);
   display.drawString(0, 40,  dispString[2]);
   display.drawString(0, 60,  dispString[3]);
   display.display();
}
void connection()
{
   WiFi.mode (WIFI_STA);
    log(0,String("connect: ") + ssid);
    WiFi.begin(ssid, password);
  }
void wifiConnect()
{
    Serial.begin(9600);
    delay(10);
    connection();
  int NAcounts=0;
  int status = WL_IDLE_STATUS;
  
  
    while (WiFi.status() != WL_CONNECTED && NAcounts < 7   ) {
        delay(500);
        Serial.println("Connecting to WiFi...");
        wifiConnect();
       
NAcounts ++;
return;

  }


  bool success = Ping.ping("www.google.com", 3);
 
 
 if(!success){
    Serial.println("Ping failed");
    Serial.println("connecting again");
    connection();
 
    // wait 5 seconds for connection:

    delay(5000);
   
  }
 
  Serial.println("Ping succesful"); 
    log(0,"ip:" + WiFi.localIP().toString());

}
void httpRequest(String p1, String p2, String message){
 
#ifndef __DEMO
    WiFiClientSecure client;
    //client.setCACert(test_root_ca);
     const int httpPort = 443;
#else 
    WiFiClient client; 
     const int httpPort = 9000;
#endif
    
    delay(20);
   
       Serial.println(host);
          Serial.println(httpPort);
    if (!client.connect(host, httpPort)) {
        Serial.println("connection failed");
       
        log(3,"msg to server: FAILED");
         connection();
        return;
    }

    // We now create a URI for the request
    String url = "/api/test/";
    url += p1 ;
    url += "/";
    url += p2;

    int msgLength = 14+message.length();

    Serial.print("Requesting URL: ");
    Serial.println(url);


    #ifndef __DEMO
        client.print(String("POST https://")+ host  + url + " HTTP/1.0\r\n" +
                 "Host: " + host + "\r\n" +
                 "Content-Type: application/json\r\n" + 
                   "Data-Type: json\r\n" + 
                 "Content-Length: "+msgLength+"\r\n" + 
                 "Connection: close\r\n\r\n" + 
                 "{\"message\":\""+message+"\"}");
    #else 
         client.print(String("POST ") + url + " HTTP/1.1\r\n" +
                 "Host: " + host + "\r\n" +
                 "Content-Type: application/json\r\n" + 
                 "Data-Type: json\r\n" + 
                 "Content-Length: "+msgLength+"\r\n" + 
                 "Connection: close\r\n\r\n" + 
                 "{\"message\":\""+message+"\"}");
    #endif

    
   
    unsigned long timeout = millis();
    while (client.available() == 0) {
        if (millis() - timeout > 5000) {
            Serial.println(">>> Client Timeout !");
            client.stop();
            return;
        }
    }
  Serial.println("-----");
    // Read all the lines of the reply from server and print them to Serial
    while(client.available()) {
        String line = client.readStringUntil('\r');
        Serial.print(line);
    }
    log(3,"msg to server: OK");
    Serial.println();
    Serial.println("closing connection");
}


unsigned int keyval=0;
String adcString;

int i;

int tick=0;

int input_1_prev=1;
int input_1_now=1;
unsigned long input_1_last_changed=millis();
unsigned long input_1_delay = 30;

void flip (){
  if ( tick == 50 ) {
    digitalWrite(13,0);
    digitalWrite(15,1);
  } else {
    digitalWrite(13,1);
    digitalWrite(15,0);
    tick=1;
     httpRequest(machine_id,"init", "");

  }
}

// ================================================ SETUP ================================================
void setup() {
  i = 0;

  dispString[0]="";
  dispString[1]="";
  dispString[2]="";
  dispString[3]="";
  // Initialises OLED display 
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_16);

   

  // Draw text on OLED display 
 
  display.display();
  delay(200); 

  wifiConnect();
       
  pinMode(14, OUTPUT);      // Q.0 relay output
  pinMode(12, OUTPUT);      // Q.1 relay output
  pinMode(13, OUTPUT);      // Q.2 relay output
  pinMode(15, OUTPUT);      // Q.3 relay output
  
  pinMode(2, OUTPUT);       // Q.4 relay output
  pinMode(33, OUTPUT);      // Q.5 relay output
  pinMode(26, OUTPUT);      // T.0 transistor output
  pinMode(27, OUTPUT);      // T.1 transistor output

  pinMode(18, INPUT);      // I.0 digital input
  pinMode(39, INPUT);      // I.1 digital input
  pinMode(34, INPUT);      // I.2 digital input
  pinMode(35, INPUT);      // I.3 digital input
  pinMode(19, INPUT);      // I.4 digital input
  pinMode(21, INPUT);      // I.5 digital input
  pinMode(22, INPUT);      // I.6 digital input
  pinMode(23, INPUT);      // I.7 digital input
  pinMode(36, INPUT);      // I.7 digital input

  digitalWrite(4, LOW);

  httpRequest(machine_id,"init", "");

 
}

char serial_buffer[256];

void loop() {

  int input_1 = digitalRead(18);

  if( input_1_prev != input_1 ) {
     Serial.print(".");
    input_1_last_changed = millis();
  }

 if ((millis() - input_1_last_changed) > 50) {
   
    if (input_1_now != input_1) {
      input_1_now = input_1;

        httpRequest(machine_id, "1", String(1-input_1_now));
    }
  }
 
  // save the reading. Next time through the loop, it'll be the lastButtonState:
  input_1_prev = input_1;
             
  log(1, String("INPUT: ") + (digitalRead(18)==1?"0":"1"));
  delay(2); 


  if(Serial.available()) {    
     int i = 0;
     while (Serial.available()>0){
       serial_buffer[i] = Serial.read();  //read Serial        
        if ( serial_buffer[i]==10 ) {
           serial_buffer[i]=0;
           break;
        }
        i++;
     }
      Serial.println(serial_buffer);
       log(2, String("rs-485: ") + String(serial_buffer));
      httpRequest(machine_id, "RS485", serial_buffer);
     
  }


}
